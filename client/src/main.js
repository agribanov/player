import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import Vuelidate from 'vuelidate'
import VueSession from 'vue-session'
import { Slider, DatePicker, Select } from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import moment from 'moment';

Vue.config.productionTip = false;
Vue.use(Vuelidate);
// Vue.use(VueSession);
Vue.use(Slider);
Vue.use(DatePicker);
Vue.use(Select);
Vue.use(moment);
Vue.use(VueSession, {persist: true})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
// city: '',
//     description: '',
//     sport: '',
//     place: '',
//     data: '',
//     price: '',
//     membersNumberLimit: null,
//     sex: '',
//     maxAge: null,
//     minAge: null,
//     phoneNumber: '',
//     regular: null,
//     closed: '',
//     period: ''
//  eventTitle
