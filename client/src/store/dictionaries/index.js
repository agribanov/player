import Vue from 'vue'

const state = {
    sports: null,
    gender: null,
    priceTypes: null,
    coverTypes: null,
    cities: null,
    // isAuthenticated: localStorage.getItem('expires_at') != null && new Date().getTime() < localStorage.getItem('expires_at')
}
const mutations = {
    // isAuthenticated: (state, isAuthenticated) => {
    //     state.isAuthenticated = isAuthenticated
    // },
    setSports: (state, sports) => {
            state.sports = sports
    },
    setGender: (state, gender) => {
        state.gender = gender
    },
    setPriceTypes: (state, priceTypes) => {
        state.priceTypes = priceTypes
    },
    setCoverTypes: (state, coverTypes) => {
        state.coverTypes = coverTypes
    },
    setCities: (state, cities) => {
        state.cities = cities
    },

}
const actions = {
    getAllCities: async (context) => {
        let url = 'https://playerplus.herokuapp.com/getCity';

        await Vue.axios.get(url)
            .then(res => {
                context.commit('setCities', res.data)
            })
            .catch(error => {
                console.log('error', error)
            })
    },
    getCoverType: async (context) => {
        let url = 'https://playerplus.herokuapp.com/getCoverType';
        await Vue.axios.get(url)
            .then(res => {
                context.commit('setCoverTypes', res.data)
            })
            .catch(error => {
                console.log('error', error)
            })

    },
    getPriceType: async (context) => {
        let url = 'https://playerplus.herokuapp.com/getPriceType';
        await Vue.axios.get(url)
            .then(res => {
                context.commit('setPriceTypes', res.data)
            })
            .catch(error => {
                console.log('error', error)
            })

    },
    getGender: async (context) => {
        let url = 'https://playerplus.herokuapp.com/getSex';
        await Vue.axios.get(url)
            .then(res => {
                context.commit('setGender', res.data)
            })
            .catch(error => {
                console.log('error', error)
            })
    },
    getAllSports: async (context) => {
        console.log('vuex')
        let url = 'https://playerplus.herokuapp.com/getSports';
        await Vue.axios.get(url)
            .then(res => {
                context.commit('setSports', res.data)

            })
            .catch(error => {
                console.log('error', error)
            })
    },

}
const getters = {
    // getSports: (state) => state.sports,

}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
