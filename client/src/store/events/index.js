import Vue from 'vue'

const state = {
    events: [],
}

const mutations = {
    setEvents: (state, events) => {
        state.events = events
    }
}

const actions = {
    getEvents: async (context) => {
        let url = 'https://playerplus.herokuapp.com/feed';
        console.log('start events')
        await Vue.axios.get(url)
            .then(res => {
                context.commit('setEvents', res.data)
                console.log('events res.data', res.data)
            })
            .catch(error => {
                console.log('error', error.response)
            })

    },
    createEvent: async (newEvent) => {
        let url = 'https://playerplus.herokuapp.com/event';
        console.log('create event')
        await Vue.axios.post(url, newEvent, {
            withCredentials: true,
        })
            .then(res => {
                console.log('create event', res)
            })
            .catch(error => {
                console.log('error', error.response)
            })
    }
}

const getters = {
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
