import Vue from 'vue'
import Swal from 'sweetalert2'

const state = {
    modalAuth: false,
    modalLogIn: false,
    modalCreateEvent: false,
    modalSportsCategory: false
}

const mutations = {
    openModalAuth: (state) => {
            state.modalAuth = true
    },
    closeModalAuth: (state) => {
        console.log('call')
        state.modalAuth = false
    },
    openModalLogIn: (state) => {
        state.modalLogIn = true
    },
    closeModalLogIn: (state) => {
        state.modalLogIn = false
    },
    openModalCreateEvent: (state) => {
        state.modalCreateEvent = true
    },
    closeModalCreateEvent: (state) => {
        state.modalCreateEvent = false
    },
    openModalSportsCategory: (state) => {
        state.modalSportsCategory = true
    },
    closeModalSportsCategory: (state) => {
        state.modalSportsCategory = false
    },

}

const actions = {
    registerNewUser: async (context, user) => {
        let url = 'https://playerplus.herokuapp.com/registration'

        let data;

        await Vue.axios
            .post(url, user)
            .then(response => {

                    Swal.fire({
                        position: "top-end",
                        type: "success",
                        title: "Вы успешно зарегистрировались",
                        showConfirmButton: false,
                        timer: 1500,
                        confirmButtonColor: "#23bfca"
                    });
                    data = response;
               // console.log("OK", response);
            })
            .catch(error => {

                if(error.response.data === 'User with such email is already exist') {

                    Swal.fire({
                        position: "top-end",
                        // type: "success",
                        title: "Пользователь с такой почтой уже зарегистрирован",
                        showConfirmButton: false,
                        timer: 1500,
                        confirmButtonColor: "#23bfca"
                    });
                }
                data = error;
                // console.log("NOT OK", error.response);
            });
        return {response: data}
    },
    logIn: async (context, user) => {
        let url = 'https://playerplus.herokuapp.com/login'
           await Vue.axios
                .post(url, user)
                .then(response => {
                    console.log("OK logIn", response.headers);
                   context.commit('closeModalAuth')
                    context.commit('closeModalLogIn')
                    context.commit('openModalSportsCategory')
                })
                .catch(error => {
                    console.log("NOT OK", error.response);
                });
    }
}

const getters = {
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
