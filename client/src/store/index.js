import Vue from 'vue'
import Vuex from 'vuex'
import VueAxios from 'vue-axios'
import dictionaries from './dictionaries/index'
import modals from './modals/index'
import events from './events/index'
import axios from 'axios'

Vue.use(VueAxios, axios)
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    dictionaries,
    modals,
    events
  }
})
