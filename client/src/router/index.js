import Vue from 'vue';
import VueRouter from 'vue-router';
import Profile from '../components/profile/Profile';
import Main from '../components/Main';
import MainInfo from "../components/profile/MainInfo";
import MainInfoEdit from "../components/profile/MainInfoEdit";
import Contacts from "../components/profile/Contacts";
import SportType from "../components/profile/SportType";
import AboutMe from "../components/profile/AboutMe";
import Favorites from '../components/profile/Favorites';
import Friends from '../components/profile/Friends';
import Chats from '../components/profile/Chats';
import Archive from '../components/profile/Archive';
import MyEvents from '../components/profile/MyEvents';
import Reference from '../components/profile/Reference';
import EventList from '../components/event-list/eventList';
import Events from '../components/event-list/Events';
import People from "../components/event-list/People";
import Places from "../components/event-list/Places";
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'EventList',
    component: EventList,
    children: [
      {
        path: '',
        name: 'Events',
        components: { default: Events },
      },
      {
        path: 'people',
        name: 'People',
        components: { default: People },
      },
      {
        path: 'places',
        name: 'Places',
        components: { default: Places },
      },

    ]
  },
  {
    path: '/main',
    name: 'main',
    component: Main
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    children: [
      {
        path: '',
        name: 'MainInfo',
        components: { default: MainInfo },
      },
      {
        path: 'edit',
        name: 'MainInfoEdit',
        components: { default: MainInfoEdit },
        // beforeEnter: InfoEdit
      },
      {
        path: 'contacts',
        name: 'Contacts',
        components: { default: Contacts }
      },
      {
        path: 'sport-type',
        name: 'SportType',
        components: { default: SportType },
        // beforeEnter: authGuard
      },
      {
        path: 'about-me',
        name: 'AboutMe',
        components: { default: AboutMe },
        // beforeEnter: authGuard
      },
      {
        path: 'favorites',
        name: 'Favorites',
        components: { default: Favorites },
        // beforeEnter: authGuard
      },
      {
        path: 'friends',
        name: 'Friends',
        components: { default: Friends },
        // beforeEnter: authGuard
      },
      {
        path: 'chats',
        name: 'Chats',
        components: { default: Chats },
        // beforeEnter: authGuard
      },
      {
        path: 'archive',
        name: 'Archive',
        components: { default: Archive },
        // beforeEnter: authGuard
      },
      {
        path: 'my-events',
        name: 'MyEvents',
        components: { default: MyEvents },
        // beforeEnter: authGuard
      },
      {
        path: 'reference',
        name: 'Reference',
        components: { default: Reference },
        // beforeEnter: authGuard
      },

    ]
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
